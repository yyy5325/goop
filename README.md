

 **如果你正从事量化交易或者对区块链&defi&机枪池有基本概念的、可以了解下该项目、该项目旨在为网络量化策略提供最佳的套利标、使网络策略的理论收益最大化。**
 **项目地址：https://gitee.com/yyy5325/smartpool** 

 
# goop

#### 1、介绍
谷粒商城-2020-Spring Cloud

用最流行的技术、写未曾踩过的BUG

@Date  2020-07-26   

高级篇完结、不定期更新文档&注释

新建dev分支、拓展开发秒杀服务

```html
1、用户session数据以常量(loginUser)存储于redis
2、下单流程优化、创建订单.订单验价流程 -->to订单确认时完成验价。
3、秒杀系统redis存储结构优化、降低缓存冗余、增加缓存灵活度
```

#### 2、项目架构

```html
1、使用Spring Cloud (Alibaba) H版技术栈支持的微服务架构商城项目、

2、所有三方组件都使用 linux/docker 容器技术提供环境支持

3、只发布后端代码、使用swagger2提供接口文档支持

4、使用Redis/Redisson 提供缓存和分布式锁支持、Session数据统一存储

5、使用ELK技术栈做全文检索、日志分析及可视化等功能

6、使用RabbitMQ 提供柔性事务保证数据最终一致性、秒杀流量削峰等功能

7、导入了人人开源项目下/renren-fast  /renren-generator  /renren-fast-vue 三个模块构建基本需求、
```

#### 3、模块声明

```xml
    <modules>
        <module>renren-generator</module>        <!--不做介绍-->
        <module>renren-fast</module>             <!--不做介绍-->
        <module>goop-common</module>             <!--通用依赖和工具类的封装-->
        <module>goop-product</module>            <!--商品模块、最为核心-->
        <module>goop-gateway</module>            <!--统一网关、路由分发、与nginx对接-->
        <module>goop-member</module>             <!--会员相关服务、-->
        <module>goop-coupon</module>             <!--商品优惠价相关服务-->
        <module>goop-ware</module>               <!--商品库存相关-->
        <module>goop-order</module>              <!--订单模块-->
        <module>goop-search</module>             <!--前台检索模块、对接ES-->
        <module>goop-login-auth</module>         <!--登录认证模块-->
        <module>goop-third-party</module>        <!--第三方服务模块-->
        <module>goop-sso-server</module>         <!--单点登录测试-服务端模块-->
        <module>goop-sso-client1</module>        <!--单点登录测试-客户端模块-->
        <module>goop-sso-client</module>         <!--单点登录测试-客户端模块-->
        <module>goop-cart</module>               <!--购物车模块-->
        <module>goop-seckill</module>            <!--秒杀模块-->
    </modules>
```

#### 4、开发环境

项目是使用Maven搭建的聚合工程、通过父POM文件控制全局依赖版本、核心依赖版本如下

```xml
<jdk.version>11</jdk.version>  

<!--spring boot 2.2.2-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-dependencies</artifactId>
    <version>2.2.2.RELEASE</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>

<!--spring cloud Hoxton.SR1-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-dependencies</artifactId>
    <version>Hoxton.SR1</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>

<!--spring cloud alibaba 2.1.0.RELEASE-->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-alibaba-dependencies</artifactId>
    <version>2.1.0.RELEASE</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
```

#### 使用说明

首页的这个文档 ---> 	want clone？？look look this .md



#### 参与贡献

1.  @liangzengguang Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
<<<<<<< HEAD
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
=======
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
>>>>>>> 500f71524fe38fb5378575a0986017fd2cc38fd5
