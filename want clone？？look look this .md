##  克隆必看

想要clone无缝运行该项目、知道以下几点是必备的。

### 1、项目整体架构

如果你能来到商城首页、那么你肯定在浏览器地址栏输⼊了goop.com、你的请求将会被nginx解析host地址、代理到goop-gateway网关、⽹关服务将根据保存的映射规则将你的请求负载均衡到合适的goop-product服务上、前提是这个服务能在Nacos 上拉取到、最后goop-product会查询Redis/MySQL将页面的动态数据返回给你、而你需要的静态数据也会自动请求Nginx服务。当然、只有这个请求是这样的。

### 2、依赖环境

项目是使用Maven搭建的聚合工程、通过父POM文件控制全局依赖版本、核心依赖版本如下

```

<jdk.version> 11 </jdk.version>

<!--spring boot 2.2.2-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-dependencies</artifactId>
    <version>2.2.2.RELEASE</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
<!--spring cloud Hoxton.SR1-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-dependencies</artifactId>
    <version>Hoxton.SR1</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
<!--spring cloud alibaba 2.1.0.RELEASE-->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-alibaba-dependencies</artifactId>
    <version>2.1.0.RELEASE</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
```

### 3、插件

为加快开发效率、本项目使用了mybatis-plus和lombok插件、缺少lombok插件将无法编译!

### 4、三方组件

![docker images](https://gitee.com/yyy5325/goop/raw/master/img/image-20200702091225087.png)

### 5、模块声明

```
    <modules>
        <module>renren-generator</module>        <!--不做介绍-->
        <module>renren-fast</module>             <!--不做介绍-->
        <module>goop-common</module>             <!--通用依赖和工具类的封装-->
        <module>goop-product</module>            <!--商品模块、最为核心-->
        <module>goop-gateway</module>            <!--统一网关、路由分发、与nginx对接-->
        <module>goop-member</module>             <!--会员相关服务、-->
        <module>goop-coupon</module>             <!--商品优惠价相关服务-->
        <module>goop-ware</module>               <!--商品库存相关-->
        <module>goop-order</module>              <!--订单模块-->
        <module>goop-search</module>             <!--前台检索模块、对接ES-->
        <module>goop-login-auth</module>         <!--登录认证模块-->
        <module>goop-third-party</module>        <!--第三方服务模块-->
        <module>goop-sso-server</module>         <!--单点登录测试-服务端模块-->
        <module>goop-sso-client</module>         <!--单点登录测试-客户端模块-->
        <module>goop-sso-client1</module>         <!--单点登录测试-客户端模块-->
        <module>goop-cart</module>               <!--购物车模块-->
    </modules>
```

### 6、想run一下？

#### 6.1、修改本地host文件、

  商城域名及子域名映射如下、我的虚拟机地址是这个

```xml
192.168.56.10  search.goop.com
192.168.56.10  goop.com
192.168.56.10  item.goop.com
```

#### 6.2、nginx.conf配置

![nginx.conf](https://gitee.com/yyy5325/goop/raw/master/img/image-20200702091248811.png)

#### 6.3、goop.conf配置

![goop.conf](https://gitee.com/yyy5325/goop/raw/master/img/image-20200702091242727.png)



